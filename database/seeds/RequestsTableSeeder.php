<?php

use Illuminate\Database\Seeder;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new App\Request;
        $request->requester_id = 3;
        $request->receiver_id = 1;

        $request->save();

        $message1 = new App\Message;
        $message1->message = 'Are you available?';
        $message1->user_id = 3;
        $message2 = new App\Message;
        $message2->message = 'yes!';
        $message2->user_id = 1;
        $message1->request()->associate($request);
        $message2->request()->associate($request);
        $message1->save();
        $message2->save();


        $request = new App\Request;
        $request->requester_id = 4;
        $request->receiver_id = 1;

        $request->save();

        $message1 = new App\Message;
        $message1->message = 'Are you available?';
        $message1->user_id = 4;
        $message2 = new App\Message;
        $message2->message = "Sorry I'm out of town";
        $message2->user_id = 1;
        $message1->request()->associate($request);
        $message2->request()->associate($request);
        $message1->save();
        $message2->save();

        $request = new App\Request;
        $request->requester_id = 5;
        $request->receiver_id = 2;

        $request->save();

        $message1 = new App\Message;
        $message1->message = 'Are you available?';
        $message1->user_id = 5;
        $message2 = new App\Message;
        $message2->message = "Sure!";
        $message2->user_id = 2;
        $message1->request()->associate($request);
        $message2->request()->associate($request);
        $message1->save();
        $message2->save();

        $request = new App\Request;
        $request->requester_id = 6;
        $request->receiver_id = 2;

        $request->save();

        $message1 = new App\Message;
        $message1->message = 'Are you available?';
        $message1->user_id = 6;
        $message2 = new App\Message;
        $message2->message = "Sorry, I have another appointment";
        $message2->user_id = 2;
        $message1->request()->associate($request);
        $message2->request()->associate($request);
        $message1->save();
        $message2->save();
    }
}
