<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 10; $i++)
        {
        	$thisUser = new App\User;
        	$thisUser->name = 'test'.$i;
        	$thisUser->email = 'test'.$i.'@test.com';
        	$thisUser->password = bcrypt('test123');
        	$thisUser->save();
        }

        $photographer = App\User::find(1);
        $profile = new App\Profile;

        $profile->description = 'Hello please contact me if you have any questions';
        $profile->price = '$30 per hour';
        $profile->professional_at = 'Wedding';
        $profile->city_id = 178;
        $profile->user()->associate($photographer);
        $profile->save();

        $photographer = App\User::find(2);
        $profile = new App\Profile;

        $profile->description = 'Hello please contact me if you have any questions';
        $profile->price = '$20 per hour';
        $profile->professional_at = 'Events';
        $profile->city_id = 163;
        $profile->user()->associate($photographer);
        $profile->save();

    }
}
