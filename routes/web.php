<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware' => 'auth'], function () {
	Route::get('/profile', 'UserController@showProfile');
	Route::post('/profile', 'UserController@saveProfile');
	Route::get('/send/request/{user_id}', 'RequestController@showRequestForm');
	Route::post('/send/request/{user_id}', 'RequestController@submitRequestForm');
	Route::get('/request/{request_id}', 'RequestController@showRequest');
	Route::post('/send/message/{request_id}', 'RequestController@sendMessage');
	Route::post('/request/accept/{request_id}', 'RequestController@acceptStatusChange');
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');
});

Auth::routes();


Route::get('/about', 'HomeController@aboutUs');
