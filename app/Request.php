<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $table = 'requests';

    public function messages()
    {
    	return $this->hasMany('App\Message', 'request_id');
    }

    public function requester()
    {
    	return $this->belongsTo('App\User', 'requester_id');
    }

    public function receiver()
    {
    	return $this->belongsTo('App\User', 'receiver_id');
    }
}
