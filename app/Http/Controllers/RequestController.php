<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RequestController extends Controller
{
    public function showRequestForm($userId)
    {
    	if(\Auth::user()->id == $userId)
    		return redirect('/profile');
    	$user = \App\User::find($userId);
    	$user->load('profile.city.state');
    	return view('requestForm')->with('user', $user);
    }

    public function submitRequestForm(Request $request, $userId)
    {
    	$this->validate($request, [
        	'message' => 'required',
    	]);
    	$thisRequest = new \App\Request;
    	$requester = \Auth::user();
    	$receiver = \App\User::find($userId);
    	

    	$thisRequest->requester()->associate($requester);
    	$thisRequest->receiver()->associate($receiver);
    	$thisRequest->save();

    	$message = new \App\Message;
    	$message->message = $request->message;
    	$message->request()->associate($thisRequest);
    	$message->user()->associate($requester);
    	$message->save();

    	return redirect('/request/'.$thisRequest->id);
    }

    public function showRequest($requestId)
    {
    	$user = \Auth::user();
    	$request = \App\Request::find($requestId);
    	$request->load('messages');
    	if($request->requester_id == $user->id)
    	{
    		$other = \App\User::find($request->receiver_id);
    		$other->load('profile.city.state');
    		return view('request')
    				->with('user', $user)
    				->with('other', $other)
    				->with('request', $request)
    				->with('perspective', 'requester');
    	}
    	else if($request->receiver_id == $user->id)
    	{
    		$other = \App\User::find($request->requester_id);
    		return view('request')
    				->with('user', $user)
    				->with('other', $other)
    				->with('request', $request)
    				->with('perspective', 'receiver');
    	}
    	else
    		return redirect('/profile');
    }

    public function sendMessage(Request $request, $requestId)
    {
    	$this->validate($request, [
        	'message' => 'required',
    	]);
    	$thisRequest = \App\Request::find($requestId);
    	if(!(\Auth::user()->id != $thisRequest->receiver_id || \Auth::user()->id != $thisRequest->requester_id))
    		return \Redirect::back()->withErrors(['msg', 'You are not receiver or requester of this request']);

    	$message = new \App\Message;
    	$message->message = $request->message;

    	$message->request()->associate($thisRequest);
    	$message->user()->associate(\Auth::user());
    	$message->save();

    	return \Redirect::back();
    }

    public function acceptStatusChange(Request $request, $requestId)
    {
    	$this->validate($request, [
        	'status' => 'required',
    	]);

    	$thisRequest = \App\Request::find($requestId);
    	if(\Auth::user()->id != $thisRequest->receiver_id)
    		return \Redirect::back()->withErrors(['msg', 'You are not receiver of this request']);

		$thisRequest->accepted = $request->status;
		$thisRequest->save();
		return \Redirect::back();

    }
}
