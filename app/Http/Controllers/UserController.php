<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

use App\State;
use App\Profile;

class UserController extends Controller
{
    public function showProfile()
    {
    	$user = Auth::user();
    	$user->load('profile', 'requestsSent.receiver', 'requestsReceived.requester');
    	$states = State::find(1)->with('cities');
        return view('profile')->with('user', $user)->with('states', $states);
    }

    public function saveProfile(Request $request)
    {
    	$this->validate($request, [
        	'professional_at' => 'required',
        	'price' => 'required',
        	'city' => 'required',
        	'intro' => 'required',
    	]);
    	$user = Auth::user();
    	$profile = Profile::firstOrNew(['user_id' => $user->id]);
    	$profile->description = $request->intro;
    	$profile->price = $request->price;
    	$profile->professional_at = $request->professional_at;
    	$profile->city_id = $request->city;

    	$profile->save();
    	return redirect('/profile');
    }
}
