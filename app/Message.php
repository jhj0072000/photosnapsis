<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    public function request()
    {
    	return $this->belongsTo('App\Request', 'request_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
