@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Send Request</h1>
                @if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3 col-md-offset-2">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/300x300" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>{{$user->name}}</h3>
                <h4>Professional: {{$user->profile->professional_at}}</h4>
                <h4>{{$user->profile->city->name}}, {{$user->profile->city->state->name}}</h4>
                <p>{{$user->profile->description}}</p>
                <form action="/send/request/{{$user->id}}" method="POST" id="request-form" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Message:</label>
                        <div class="col-lg-8">
                            <textarea name="message" rows="4" cols="50"></textarea>
                        </div>
                    </div>
                </form>
            	<button class="btn btn-primary" type="submit" form="request-form" value="Submit">Submit<span class="glyphicon glyphicon-chevron-right"></button>
            </div>
        </div>
        <!-- /.row -->



</div>
@endsection
