@extends('layouts.app')

@section('content')
<div class="container">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Request</h1>
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

			</div>
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-md-3 col-md-offset-2">
				<a href="#">
					<img class="img-responsive" src="http://placehold.it/300x300" alt="">
				</a>
			</div>
			<div class="col-md-5">
				<h3>{{$other->name}}</h3>
				<h4>{{$other->email}}</h4>
				<h4>{{$request->created_at}}</h4>
				@if($perspective == 'requester')
					@if($request->accepted == 0)
						<h4>Waiting for response</h4>
					@elseif($request->accepted == 1)
						<h4>Request Declined</h4>
					@else($request->accepted == 2)
						<h4>Request Accepted</h4>
					@endif
					<h4>Professional: {{$other->profile->professional_at}}</h4>
					<h4>{{$other->profile->city->name}}, {{$other->profile->city->state->name}}</h4>
					<p>{{$other->profile->description}}</p>
				@else
					<form action="/request/accept/{{$request->id}}" method="POST" id="approve-form" class="form-inline">
						{{ csrf_field() }}
					@if($request->accepted == 0)
						<label class="radio-inline"><input type="radio" name="status" value="1">Decline Request</label>
						<label class="radio-inline"><input type="radio" name="status" value="2">Accept Request</label>
					@elseif($request->accepted == 1)
						<label class="radio-inline"><input type="radio" name="status" value="1" checked>Decline Request</label>
						<label class="radio-inline"><input type="radio" name="status" value="2">Accept Request</label>
					@else($request->accepted == 2)
						<label class="radio-inline"><input type="radio" name="status" value="1">Decline Request</label>
						<label class="radio-inline"><input type="radio" name="status" value="2" checked>Accept Request</label>
					@endif
						<button class="btn btn-primary btn-xs" type="submit" form="approve-form" value="Submit">Submit</button>
					</form>
				@endif
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3 class="col-md-offset-2">Messages</h3>
				@foreach($request->messages as $message)
					@if($message->user_id == $user->id)
						<p class="my-message col-md-offset-2"><b>{{$user->name}}: </b>{{$message->message}}</p>
					@else
						<p class="others-message col-md-offset-2"><b>{{$other->name}}: </b>{{$message->message}}</p>
					@endif
				@endforeach
				<form action="/send/message/{{$request->id}}" method="POST" id="request-form" class="form-inline col-md-offset-1">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-lg-2 control-label">Message</label>
						<div class="col-lg-8">
							<textarea name="message" rows="2" cols="50"></textarea>
						</div>
					</div>
					<button class="btn btn-primary" type="submit" form="request-form" value="Submit">Send</button>
				</form>
			</div>
		</div>
		<!-- /.row -->



</div>
@endsection
