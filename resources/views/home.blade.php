@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Photographers</h1>
            </div>
        </div>
        <!-- /.row -->
        @foreach($profiles as $profile)
            <div class="row">
                <div class="col-md-3 col-md-offset-2">
                    <a href="#">
                        <img class="img-responsive" src="http://placehold.it/300x300" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>{{$profile->user->name}}</h3>
                    <h4>Professional: {{$profile->professional_at}}</h4>
                    <h4>{{$profile->city->name}}, {{$profile->city->state->name}}</h4>
                    <p>{{$profile->description}}</p>
                    @if(\Auth::user()->id == $profile->user->id)
                        <a class="btn btn-primary" href="/profile">Show My Profile <span class="glyphicon glyphicon-chevron-right"></span></a>
                    @else
                        <a class="btn btn-primary" href="/send/request/{{$profile->user->id}}">Send Request <span class="glyphicon glyphicon-chevron-right"></span></a>
                    @endif
                </div>
            </div>
            <hr>
        @endforeach
        <!-- /.row -->



</div>
@endsection
