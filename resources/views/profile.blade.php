@extends('layouts.app')

@section('content')


<div class="container" style="margin-top:100px;">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-10">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Account</a></li>
                <li><a data-toggle="tab" href="#menu1">Requests Received</a></li>
                <li><a data-toggle="tab" href="#menu2">Requests Sent</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h3>Account</h3>
                    <form action="/profile" method="POST" id="account-form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Name:</label>
                            <div class="col-lg-8">
                                <p class="account-info-space">{{$user->name}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Email:</label>
                            <div class="col-lg-8">
                                <p class="account-info-space">{{$user->email}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Joined:</label>
                            <div class="col-lg-8">
                                <p class="account-info-space">{{$user->created_at}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Professional At:</label>
                            <div class="col-lg-8">
                                @if($user->profile != NULL)
                                    <input name="professional_at" class="form-control" value="{{$user->profile->professional_at}}" type="text">
                                @else
                                    <input name="professional_at" class="form-control" value="" type="text">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Price:</label>
                            <div class="col-lg-8">
                                @if($user->profile != NULL)
                                    <input name="price" class="form-control" value="{{$user->profile->price}}" type="text">
                                @else
                                    <input name="price" class="form-control" value="" type="text">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Location:</label>
                            <div class="col-lg-8">
                                <select name="state">
                                    <option value="{{$states->first()->id}}">{{$states->first()->name}}</option>
                                </select>
                                <select name="city">
                                    @if($user->profile == NULL)
                                        @foreach($states->first()->cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    @else
                                        @foreach($states->first()->cities as $city)
                                            @if($user->profile->city_id == $city->id)
                                                <option value="{{$city->id}}" selected>{{$city->name}}</option>
                                            @else
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Intro:</label>
                            <div class="col-lg-8">
                                @if($user->profile != NULL)
                                    <textarea name="intro" rows="4" cols="50">{{$user->profile->description}}</textarea>
                                @else
                                    <textarea name="intro" rows="4" cols="50"></textarea>
                                @endif
                            </div>
                        </div>
                    </form>
                    <button class="col-md-offset-2 btn btn-primary" type="submit" form="account-form" value="Submit">Submit<span class="glyphicon glyphicon-chevron-right"></button>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Requests Received</h3>
                    <table class="table table-responsive">
                        <thead>
                            <tr>You have <span>{{$user->requestsReceived->count()}}</span> requests received</tr>
                            <tr>
                                <td>From</td>
                                <td>Accepted</td>
                                <td>Date Received</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->requestsReceived as $received)
                                <tr data-href="/request/{{$received->id}}" style='cursor:pointer'>
                                    <td>{{$received->requester->name}}</td>
                                    @if($received->accepted == 0)
                                        <td>Waiting for response</td>
                                    @elseif($received->accepted == 1)
                                        <td>Request Declined</td>
                                    @else($received->accepted == 2)
                                        <td>Request Accepted</td>
                                    @endif
                                        <td>{{$received->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3>Requests Sent</h3>
                    <table class="table table-responsive">
                        <thead>
                            <tr>You have <span>{{$user->requestsSent->count()}}</span> requests received</tr>
                            <tr>
                                <td>To</td>
                                <td>Accepted</td>
                                <td>Date Sent</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->requestsSent as $sent)
                                <tr data-href="/request/{{$sent->id}}" style='cursor:pointer'>
                                    <td>{{$sent->receiver->name}}</td>
                                    @if($sent->accepted == 0)
                                        <td>Waiting for response</td>
                                    @elseif($sent->accepted == 1)
                                        <td>Request Declined</td>
                                    @else($sent->accepted == 2)
                                        <td>Request Accepted</td>
                                    @endif
                                        <td>{{$sent->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

